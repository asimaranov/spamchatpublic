let message_template = '<div class="chat-message" message_id="{message_id}"><div class="content"><div class="author">{username}</div>{text}</div></div>';


class Message {
    constructor(id, username, text, date, author_color, sender_type) {
        this.id = id;
        this.username = username;
        this.text = text;
        this.date = date;
        this.author_color = author_color;
        this.sender_type = sender_type;

    }

    toHtml(){
        return message_template
            .replace('{message_id}', this.id)
            .replace('{username}', this.username)
            .replace('{text}', this.text)
            .replace('{author_color}', this.author_color)
            .replace('{sender_type}', this.sender_type);
    }

    static FromArgs(args) {
        return new Message(args[0], args[1], args[2], args[3], args[4], args[5])
    }
}

class MessagesManager {

    //message: [id, username, text, date, author_color, sender_type]
    static addMessages(messages) {
        let messages_block = $(".messages_block");

        messages.forEach(function (item) {
                let message = Message.FromArgs(item);

                let messages = messages_block.children();
                let last_message_id = parseInt(messages.first().attr("message_id"));
                let first_message_id = parseInt(messages.last().attr("message_id"));

                if (isNaN(first_message_id)){
                    messages_block.append(message.toHtml())
                }

                else if(message.id < first_message_id){
                    messages_block.append(message.toHtml())
                }
                else if(message.id > last_message_id){
                    messages_block.prepend(message.toHtml())
                }
                else {
                    // TODO сделать вставку
                }



            }
        );
    }

    static sendMessage() {
        let text_field = $('.text_entry');
        let text = text_field.val();
        text_field.val('');
        text_field.trigger('focus');
        Android.sendMessage(text);
    }
}