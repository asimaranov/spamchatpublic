package com.example.andrey.spamchat.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.andrey.spamchat.R
import com.example.andrey.spamchat.model.ChatRoom
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.example.andrey.spamchat.database.Server


class ServersHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(server: Server, clickListener: (Server) -> Unit) {
        itemView.findViewById<TextView>(R.id.serverIpTextView).text = server.ip

        itemView.setOnClickListener { clickListener(server)}
    }

}

class ServersAdapter(var context: Context, var servers: ArrayList<Server>, val clickListener: (Server) -> Unit) : Adapter<ServersHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServersHolder {
        return ServersHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_server,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return servers.size
    }

    fun addItem(server: Server){
        if (servers.all { it.id != server.id }){

            servers.add(server)
            notifyDataSetChanged()
        }

    }

    override fun onBindViewHolder(holder: ServersHolder, position: Int) {
        holder.bind(servers[position], clickListener)
    }


}

