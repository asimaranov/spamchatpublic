package com.example.andrey.spamchat.utils

import androidx.fragment.app.Fragment
import com.example.andrey.spamchat.R
import com.example.andrey.spamchat.RoomsFragment
import com.example.andrey.spamchat.ServersFragment
import com.example.andrey.spamchat.SettingsFragment

enum class BottomNavigationItem(val position: Int, val id: Int) {
    SERVERS(0, R.id.navigation_servers),
    ROOMS(1, R.id.navigation_rooms),
    SETTINGS(2, R.id.navigation_settings)
}

fun findNavigationItemById(id: Int): BottomNavigationItem = when (id) {
    BottomNavigationItem.SERVERS.id -> BottomNavigationItem.SERVERS
    BottomNavigationItem.ROOMS.id -> BottomNavigationItem.ROOMS
    BottomNavigationItem.SETTINGS.id -> BottomNavigationItem.SETTINGS
    else -> BottomNavigationItem.SERVERS
}

fun BottomNavigationItem.createFragment(): Fragment = when (this) {
     BottomNavigationItem.SERVERS -> ServersFragment.newInstance()
     BottomNavigationItem.ROOMS -> RoomsFragment.newInstance()
     BottomNavigationItem.SETTINGS -> SettingsFragment.newInstance()
}