package com.example.andrey.spamchat.api


import android.util.Log
import androidx.room.Database
import com.example.andrey.spamchat.database.AppDatabase
import com.example.andrey.spamchat.database.Server
import com.example.andrey.spamchat.model.*

import java.io.ByteArrayOutputStream
import java.io.DataInputStream
import java.io.DataOutputStream
import java.lang.Exception
import java.net.InetSocketAddress
import java.net.Socket
import kotlin.math.log


enum class OutgoingPacketTypes(val code: Byte) {
    HandshakeRequest(0),
    NewMessageRequest(2),
    MessageHistoryRequest(4),
    GetRoomListRequest(6),
    KeepAlive(8)
}

enum class IncomePacketTypes(val code: Byte) {
    IncomeMessageRequest(3),
    DisconnectInfoRequest(5),
    FileTransferringRequest(7),
    MessagesHistoryRequest(9),
    RoomsListRequest(10),
    NewRoomRequest(13),
    RemoveRoomRequest(15),

}


class OutgoingPacket(var type: OutgoingPacketTypes, payload: ByteArray = ByteArray(0)) {

    var outputStream = ByteArrayOutputStream()
    var packetWriter: DataOutputStream
    val payload: ByteArray
        get() = outputStream.toByteArray()

    val data: ByteArray
        get() = ByteArray(1) { this.type.code } + this.payload

    init {
        this.outputStream.write(payload)
        packetWriter = DataOutputStream(outputStream)
    }


    fun send(writer: DataOutputStream) {

        val hex = this.data.joinToString("") { String.format("%02X", it) }
        Log.d("PacketManager", "Отправлен пакет: Тип: ${this.type}, HEX: $hex")
        writer.write(this.data)
    }

}

class ApiReader(var reader: DataInputStream, var server: Server) {

    private fun processIncomeMessage() {
        val flags = reader.readByte().toInt()
        val messageId = reader.readInt()
        val roomId = reader.readShort()
        val timestamp = reader.readInt()
        val usernameLen = reader.readByte().toInt()
        val usernameBytes = ByteArray(usernameLen)
        reader.read(usernameBytes)
        val username = usernameBytes.toString(Charsets.UTF_8)
        val textLen = reader.readInt()
        val textBytes = ByteArray(textLen)
        reader.read(textBytes)
        val text = textBytes.toString(Charsets.UTF_8)
        val message = Message(
            id = messageId,
            html = text,
            room_id = roomId,
            username = username,
            is_system = flags and 0b01,
            timestamp = timestamp
        )
        MessagesManager.addMessage(message)
    }

    private fun processDisconnectInfo() {
        val reasonSize = reader.readShort()
        val reasonBytes = ByteArray(reasonSize.toInt())
        reader.read(reasonBytes)
    }

    private fun processMessageHistory() {
        val messagesCount = reader.readShort()
        for (i in 1..messagesCount) {
            processIncomeMessage()
        }
    }

    private fun processRoomsList() {
        val roomsCount = reader.readShort()

        for (i in 1..roomsCount) {
            processNewRoom()
        }
    }

    private fun processNewRoom() {
        val roomId = reader.readShort()
        val nameLen = reader.readShort()
        val nameBytes = ByteArray(nameLen.toInt())
        reader.read(nameBytes)
        val room = ChatRoom(id = roomId, name = nameBytes.toString(Charsets.UTF_8))
        RoomsManager.addRoom(room)
    }

    private fun processRemoveRoom() {
        val roomId = reader.readShort()
        RoomsManager.removeRoom(roomId)

    }

    fun listen() {
        Log.d("PacketManager", "Listening")
        val packetType = reader.readByte()
        Log.d("PacketManager", "Received packet with type $packetType")

        when (packetType) {
            IncomePacketTypes.IncomeMessageRequest.code -> processIncomeMessage()
            IncomePacketTypes.DisconnectInfoRequest.code -> processDisconnectInfo()
            IncomePacketTypes.MessagesHistoryRequest.code -> processMessageHistory()
            IncomePacketTypes.RoomsListRequest.code -> processRoomsList()
            IncomePacketTypes.NewRoomRequest.code -> processNewRoom()
            IncomePacketTypes.RemoveRoomRequest.code -> processRemoveRoom()
            else -> {
                Log.w("PacketManager", "Unknown packet with type: $packetType")

            }
        }

    }
}


class ApiWriter(var writer: DataOutputStream, var server: Server ) {


    fun requestHandshake() {

        val packet = OutgoingPacket(type = OutgoingPacketTypes.HandshakeRequest)
        val packetWriter = packet.packetWriter
        packetWriter.writeByte(server.nick.toByteArray().size)
        packetWriter.writeBytes(server.nick)
        packetWriter.writeByte(server.password.toByteArray().size)
        packetWriter.writeBytes(server.password)
        packet.send(this.writer)
    }



    fun requestNewMessage(message: OutgoingMessage) {
        val packet = OutgoingPacket(type = OutgoingPacketTypes.NewMessageRequest)
        val packetWriter = packet.packetWriter

        packetWriter.writeByte(0)  // флаги в топку
        packetWriter.writeInt(0)  // timestamp в топку

        packetWriter.writeShort(message.room_id.toInt())
        packetWriter.writeInt(message.html.toByteArray().size)

        packetWriter.write(message.html.toByteArray(Charsets.UTF_8))
        packet.send(this.writer)

    }

    fun requestMessageHistory(roomId: Short, startMessageId: Int = 0, messageCount: Int = 150) {
        val packet = OutgoingPacket(type = OutgoingPacketTypes.MessageHistoryRequest)
        val packetWriter = packet.packetWriter
        packetWriter.writeShort(roomId.toInt())
        packetWriter.writeInt(startMessageId)
        packetWriter.writeShort(messageCount)
        packet.send(this.writer)
    }

    fun requestRoomList() {
        val packet = OutgoingPacket(type = OutgoingPacketTypes.GetRoomListRequest)
        packet.send(this.writer)

    }

    fun requestKeepAlive(){
        val packet = OutgoingPacket(type = OutgoingPacketTypes.KeepAlive)
        packet.send(this.writer)
    }

}


object ApiManager {

    private var listenerThread: Thread? = null

    var apiWriter: ApiWriter? = null
    private var apiReader: ApiReader? = null
    private var socket: Socket? = null


    fun connect(server: Server, onSuccess: () -> Unit, onFailure: (Exception) -> Unit) {
        disconnect()
        Thread {
            var wasException = false

            try {
                socket = Socket()
                socket!!.connect(InetSocketAddress(server.ip, server.port), 5000)
                val reader = DataInputStream(socket!!.getInputStream())
                val writer = DataOutputStream(socket!!.getOutputStream())

                apiWriter = ApiWriter(writer = writer, server = server)
                apiReader = ApiReader(reader = reader, server = server)

                listenerThread = Thread {

                    while (true) {
                        try {
                            apiReader!!.listen()
                        } catch (e: Exception){
                            Log.d("PacketManager", "Disconnect: ${e.message}")
                        }

                    }
                }
                listenerThread!!.start()

                Thread{
                    while (true) {
                        Thread.sleep(30_000)
                        apiWriter!!.requestKeepAlive()
                    }
                }.start()

                apiWriter!!.requestHandshake()
                apiWriter!!.requestRoomList()
            } catch (e: Exception) {
                wasException = true
                onFailure(e)
            }
            if(!wasException){
                onSuccess()
            }


        }.start()
    }
    fun disconnect(){
        listenerThread?.interrupt()
        socket?.close()
        MessagesManager.messages.clear()
        RoomsManager.rooms.clear()

    }
}


object MessagesManager {

    var messages = ArrayList<Message>(0) // TODO: load messages from cache
    var messagesUpdateCallbacks = ArrayList<(Message) -> Unit>(0)

    fun addMessagesUpdateCallback(callback: (Message) -> Unit) {
        messagesUpdateCallbacks.add(callback)
    }

    fun getChatRoomMessagesHistory(roomId: Short): ArrayList<Message>{
        return ArrayList(messages.filter { it.room_id == roomId}.sortedBy { it.timestamp })
    }



    fun addMessage(message: Message) {
        messages = ArrayList(messages.filter { it.id != message.id || it.room_id != message.room_id})
        messages.add(message)
        messagesUpdateCallbacks.forEach { it(message) }

    }
}

object RoomsManager {
    var rooms = ArrayList<ChatRoom>(0)
    var roomsUpdateCallbacks = ArrayList<(ChatRoom) -> Unit>(0)
    fun addRoomsUpdateCallback(callback: (ChatRoom) -> Unit) {
        roomsUpdateCallbacks.add(callback)
    }

    fun addRoom(room: ChatRoom) {
        rooms = ArrayList(rooms.filter { it.id != room.id })
        rooms.add(room)

        roomsUpdateCallbacks.forEach {
            it(room)
        }
    }
    fun removeRoom(roomId: Short){
        rooms = ArrayList(rooms.filter { it.id != roomId })
    }
}

object ServersManager {

    var servers = ArrayList<Server>(0)

    init {
        servers = ArrayList(AppDatabase.getInstance().serverDAO().all)
    }


    var serversUpdateCallbacks = ArrayList<(Server) -> Unit>(0)

    fun addServersUpdateCallback(callback: (Server) -> Unit) {
        serversUpdateCallbacks.add(callback)
    }

    fun addServer(server: Server) {
        servers.add(server)
        AppDatabase.getInstance().serverDAO().insert(server)
        serversUpdateCallbacks.forEach { it(server) }
    }
}