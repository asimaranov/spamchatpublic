package com.example.andrey.spamchat.model


data class ChatRoom(var id: Short?, var name: String)




data class Message(
    var id: Int,
    var html: String,
    var username: String,
    var room_id: Short,
    var is_system: Int = 0,
    var timestamp: Int? = null
)

data class OutgoingMessage(
    var html: String,
    var room_id: Short

)
