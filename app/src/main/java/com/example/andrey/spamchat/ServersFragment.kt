package com.example.andrey.spamchat

import android.graphics.drawable.ClipDrawable.HORIZONTAL
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.andrey.spamchat.adapters.RoomsAdapter
import com.example.andrey.spamchat.adapters.ServersAdapter
import com.example.andrey.spamchat.api.ApiManager
import com.example.andrey.spamchat.api.ServersManager
import com.example.andrey.spamchat.database.Server
import com.example.andrey.spamchat.utils.BottomNavigationItem
import com.example.andrey.spamchat.utils.createFragment
import com.google.android.material.floatingactionbutton.FloatingActionButton
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.fragment_servers.view.*
import org.jetbrains.anko.runOnUiThread
import java.lang.Error
import java.lang.Exception


class ServersFragment : androidx.fragment.app.Fragment() {
    private lateinit var serversAdapter: ServersAdapter
    private lateinit var viewOfLayout: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewOfLayout = inflater.inflate(R.layout.fragment_servers, container, false)
        val floatingActionButton = viewOfLayout.findViewById<View>(R.id.floatingActionButton)

        floatingActionButton.setOnClickListener {
            displayAddServerDialog()
        }

        val recyclerView: RecyclerView = viewOfLayout.findViewById(R.id.serversRecyclerView)
        recyclerView.layoutManager = LinearLayoutManager(viewOfLayout.context)

        val itemDecor = DividerItemDecoration(viewOfLayout.context, HORIZONTAL)
        val dividerDrawable = ContextCompat.getDrawable(viewOfLayout.context, R.drawable.divider)!!

        itemDecor.setDrawable(dividerDrawable)
        recyclerView.addItemDecoration(itemDecor)

        serversAdapter = ServersAdapter(viewOfLayout.context, ServersManager.servers) { onServerClick(it) }

        recyclerView.adapter = serversAdapter

        ServersManager.addServersUpdateCallback {
            viewOfLayout.context.runOnUiThread { serversAdapter.addItem(it) }
        }

        return viewOfLayout
    }

    private fun onServerConnected() {
        viewOfLayout.context.runOnUiThread {
            (activity as MainActivity).switchNavigationItem(BottomNavigationItem.ROOMS)
        }
    }

    private fun onServerConnectionError(error: Exception) {
        viewOfLayout.context.runOnUiThread {
            Toasty.warning(this, "Error: ${error.message}").show()

        }
    }

    private fun onServerClick(server: Server) {
        ApiManager.connect(server, {
            onServerConnected()
        },
            {
                onServerConnectionError(it)
            })
    }

    fun displayAddServerDialog() {
        val alert = AlertDialog.Builder(activity!!)

        val dialog = alert.create()

        val view = layoutInflater.inflate(R.layout.dialog_add_room, null)

        view.findViewById<Button>(R.id.auth_go_button).setOnClickListener {
            val ip = view.findViewById<TextView>(R.id.authIPTextBox).text.toString()
            val nick = view.findViewById<TextView>(R.id.authNickTextBox).text.toString()
            val server = Server(ip = ip, nick = nick)



            ApiManager.connect(server,
                {
                    viewOfLayout.context.runOnUiThread {
                        Toasty.info(this, "Ok!").show()
                        ServersManager.addServer(server)
                        dialog.dismiss()
                    }
                },
                {
                    onServerConnectionError(it)
                })
        }

        dialog.setView(view)
        dialog.show()

    }


    companion object {
        fun newInstance() = ServersFragment()
    }
}
