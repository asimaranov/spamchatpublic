package com.example.andrey.spamchat.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface ServerDao{

    @get:Query("SELECT * FROM server")
    val all: List<Server>

    @Insert
    fun insert(server: Server)

    @Delete
    fun delete(server: Server)

}