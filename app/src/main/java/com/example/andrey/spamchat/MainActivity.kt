package com.example.andrey.spamchat

import androidx.fragment.app.Fragment
import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import com.example.andrey.spamchat.utils.BottomNavigationItem
import com.example.andrey.spamchat.utils.createFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var toolbar: ActionBar
    var currentNavigationItem: BottomNavigationItem? = null

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

        when (item.itemId) {
            R.id.navigation_servers -> {
                openFragment(ServersFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_rooms -> {
                openFragment(RoomsFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_settings -> {
                openFragment(SettingsFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbar = supportActionBar!!
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        switchNavigationItem(BottomNavigationItem.SERVERS)
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.main_container, fragment)
        transaction.commit()
    }

    fun switchNavigationItem(item: BottomNavigationItem){
        if (currentNavigationItem != item){
            currentNavigationItem = item
            openFragment(item.createFragment())
            navigation.selectedItemId = item.id

        }

    }
}