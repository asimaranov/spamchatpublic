package com.example.andrey.spamchat.utils

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.util.Log

import android.webkit.WebView
import com.example.andrey.spamchat.model.Message

@TargetApi(Build.VERSION_CODES.KITKAT)
fun WebView.addMessages(messages: ArrayList<Message>) {

    for(chunk in messages.chunked(15)){

        val jsCode = chunk.joinToString(
            prefix = "MessagesManager.addMessages([",
            postfix = "])"
        ) { "['${it.id}', '${it.username}', '${it.html.replace("'", "\'").replace("\\", "\\\\").replace("\n", "<br>")}']" }
        Log.d("HTML", jsCode)
        this.evaluateJavascript(jsCode) { Log.d("HTML", it) }

    }


}

fun WebView.addMessage(message: Message) {
    this.addMessages(arrayListOf(message))
}


