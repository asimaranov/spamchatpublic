package com.example.andrey.spamchat.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.andrey.spamchat.R
import com.example.andrey.spamchat.model.ChatRoom
import kotlinx.android.synthetic.main.item_chat.view.*


class RoomsHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {

    fun bind(room: ChatRoom, clickListener: (ChatRoom) -> Unit) {
        itemView.textViewRoomName.text = room.name

        itemView.setOnClickListener { clickListener(room)}
    }

}

class RoomsAdapter(var context: Context, var rooms: ArrayList<ChatRoom>, val clickListener: (ChatRoom) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<RoomsHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoomsHolder {
        return RoomsHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_chat,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return rooms.size
    }

    fun addItem(room: ChatRoom){
        if (rooms.all { it.id != room.id }){

            rooms.add(room)
            Log.d("PacketManager", "COUNT: ${this.itemCount}")
            notifyDataSetChanged()
        }

    }

    override fun onBindViewHolder(holder: RoomsHolder, position: Int) {
        holder.bind(rooms[position], clickListener)
    }


}

