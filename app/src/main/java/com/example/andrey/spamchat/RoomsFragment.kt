package com.example.andrey.spamchat

import android.content.Intent
import android.graphics.drawable.ClipDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.andrey.spamchat.adapters.RoomsAdapter
import com.example.andrey.spamchat.api.ApiManager
import com.example.andrey.spamchat.api.RoomsManager
import com.example.andrey.spamchat.model.ChatRoom
import org.jetbrains.anko.runOnUiThread


class RoomsFragment : androidx.fragment.app.Fragment() {
    private lateinit var viewOfLayout: View
    lateinit var roomsAdapter: RoomsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewOfLayout = inflater.inflate(R.layout.fragment_rooms, container, false)

        val recyclerView: RecyclerView = viewOfLayout.findViewById(R.id.roomsRecyclerView)
        recyclerView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(viewOfLayout.context)

        val itemDecor = DividerItemDecoration(viewOfLayout.context, ClipDrawable.HORIZONTAL)
        val dividerDrawable = ContextCompat.getDrawable(viewOfLayout.context, R.drawable.divider)!!

        itemDecor.setDrawable(dividerDrawable)
        recyclerView.addItemDecoration(itemDecor)

        roomsAdapter = RoomsAdapter(viewOfLayout.context, RoomsManager.rooms) { onChatRoomClick(it) }

        recyclerView.adapter = roomsAdapter

        RoomsManager.addRoomsUpdateCallback {
            viewOfLayout.context.runOnUiThread { roomsAdapter.addItem(it) }

        }

        return viewOfLayout
    }

    private fun onChatRoomClick(room: ChatRoom) {

    }

    companion object {
        fun newInstance() = RoomsFragment()
    }
}