package com.example.andrey.spamchat.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Server constructor(var ip: String, var port: Int=8867, var password: String="", var nick: String) {
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null

}